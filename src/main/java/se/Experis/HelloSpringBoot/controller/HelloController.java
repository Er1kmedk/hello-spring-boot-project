package se.Experis.HelloSpringBoot.controller;

import org.springframework.web.bind.annotation.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class HelloController {


    @GetMapping("/greetings")
    public String greetFriend(@RequestParam String userInput) {
        return "Greetings fräänd " + userInput + "!";
    }

    @RequestMapping(value = "/reverse", method = GET)
    public String reverseInput(@RequestParam String userInput) {
        return reverseString(userInput);
    }

    /* A private method for reversing the string keeps above mapping-
     methods only focused on sending responses.
     For bigger applications a separate class should have been considered. */
    private String reverseString(String userInput) {
        if (userInput == null) {
            return null;
        }
        else {
            StringBuilder stringBuilder = new StringBuilder(userInput).reverse();
            return stringBuilder.toString();
        }
    }
}

